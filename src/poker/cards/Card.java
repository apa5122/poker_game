package poker.cards;

import java.io.Serializable;
import java.util.HashMap;

public class Card implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3508837566383584624L;
	private int value;
	private String suit;
	
	// Numeric values for face cards
	private static final HashMap<Integer, String> values = new HashMap<Integer, String>();
	static
	{
		values.put(11, "jack");
		values.put(12, "queen");
		values.put(13, "king");
		values.put(14, "ace");
	}
	
	public Card(int value, String suit)
	{
		this.value = value;
		this.suit = suit;
	}
	
	public boolean isSameSuit(Card other)
	{
		return this.suit == other.suit;
	}
	
	public String toString()
	{
		String strValue = String.valueOf(value);
		
		if (values.containsKey(value))
			strValue = values.get(value);
		
		return strValue + " of " + suit;
	}
	
	public int getValue()
	{
		return value;
	}
}