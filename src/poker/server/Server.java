package poker.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

import poker.cards.Hand;


public interface Server extends Remote
{
	public int connect(String name) throws RemoteException;
	
	public void resend(long sequence) throws RemoteException;
		
	public long nextSequence() throws RemoteException;
	
	public boolean enoughPlayers() throws RemoteException;
	
	public void ready(int pid) throws RemoteException;
	
	public Hand dealHand(int pid) throws RemoteException;
}