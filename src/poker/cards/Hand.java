package poker.cards;

import java.io.Serializable;
import java.util.Arrays;

public class Hand implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4382402868703505289L;
	private Card[] cards = new Card[5];
	
	public Hand(Card[] cards)
	{
		this.cards = cards;
	}
	
	public int compareTo(Hand other)
	{
		if (numValue() > other.numValue())
			return 1;
		else if (numValue() < other.numValue())
			return 2;
		else
			return 3;
	}
	
	public String toString()
	{
		String hand = new String();
		
		for (int i = 0; i < cards.length; i++)
			hand += (i + 1) + ": " + cards[i].toString() + "\n";
		
		return hand;
	}
	
	private double numValue()
	{
		int[] values = sortedValues();
		double result = 0;
		
		// Check for four of a kind
		if ((values[0] == values[3]) || (values[1] == values[4]))
			result = 7 + (values[2]/16);
		// Check for three of a kind
		else if ((values[0] == values[2]) || (values[1] == values[3]) ||
				(values[2] == values[4]))
		{
			boolean pairCheck = true;
			
			// Check if the remaining two cards make up a pair
			for (int a = 0; a < 4; a++)
				if (!(values[a] == values[a + 1]))
					pairCheck = false;
			
			// Full house
			if (pairCheck == true)
				result = 6 + (values[4]/16);
			// Three of a kind
			else
				result = 3 + (values[2]/16);
		}	
		else
		{
			double highPair = 0;
			int pairs = 0;
			
			for (int k = 1; k < 5 ; k++)
				if (values[k-1] == values[k])
				{
					pairs++;
					
					if (highPair < values[k])
						highPair = values[k];
				}
			if (pairs == 2)
				result = 2 + (highPair/16);
		 	else if (pairs == 1)
		 		result = 1 + (highPair/16);
		 	else
		 	{
		 		// Check for straight
				if (((values[0] + 1) == values[1]) && ((values[1] + 1) == values[2]) &&
						((values[2] + 1) == values[3]) && ((values[3] + 1) == values[4]))
				{
					if (allSameSuit()) {
						if (values[4] == 14)
							result = 9;
						else
							result = 8 + (values[4]/16);
					}
					else
						result = 4 + (values[4]/16);
				}
				else if (allSameSuit())
					result = 5 + (values[4]/16);
				else
					result = 0 + (values[4]/16);
			}
		}

		return result;
	}
	
	private int[] sortedValues()
	{
		int numValues[] = new int[5];
		
		for(int i = 0; i < 5; i++)
		{
			numValues[i] = cards[i].getValue();
		}
		
		Arrays.sort(numValues);
		
		return numValues;
	}
	
	private boolean allSameSuit()
	{
		for(int i = 1; i < 5; i++)
			if (!cards[0].isSameSuit(cards[i]))
				return false;
		
		return true;
	}
}