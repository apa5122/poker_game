package poker.server;



public class MyServer
{
	public static void main(String[] args) throws Exception
	{
		@SuppressWarnings("unused")
		ServerImpl server;
		
		if (args.length == 1) {
			int rmiPort = Integer.parseInt(args[0]);
			server = new ServerImpl(rmiPort);
		}
		else
			System.out.println("Wrong number of arguments!");
	}
}