package poker.cards;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import poker.cards.Deck;

public class TestDeck
{
	Deck deck;
	Card[] cards;
	
	@Before
	public void setUp()
	{
		deck = new Deck();
		cards = deck.getCards();
	}
	
	@Test
	public void testGetCards()
	{
		assertEquals(52, cards.length);
		assertEquals("2 of clubs", cards[0].toString());
		assertEquals("ace of spades", cards[51].toString());
	}
}