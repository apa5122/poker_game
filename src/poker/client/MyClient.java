package poker.client;




public class MyClient
{
	public static void main(String[] args) throws Exception
	{
		if (args.length == 2)
		{
			int rmiPort = Integer.parseInt(args[0]);
			String name = args[1];
			Client client = new Client(rmiPort);
			client.connect(name);
		}
		else
			System.out.println("Wrong number of arguments!");
	}
}