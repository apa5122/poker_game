package poker.cards;

import static org.junit.Assert.*;

import org.junit.Test;


public class TestCard
{	
	Card card;
	
	@Test
	public void testToString()
	{
		card = new Card(1, "clubs");
		assertEquals("1 of clubs", card.toString());
		
		card = new Card(14, "spades");
		assertEquals("ace of spades", card.toString());
	}
	
	public void testIsSameSuit()
	{
		card = new Card(3, "spades");
		Card sameSuit = new Card(2, "spades");
		Card otherSuit = new Card(2, "hearts");
		
		assertEquals(true, card.isSameSuit(sameSuit));
		assertEquals(false, card.isSameSuit(otherSuit));
	}
}