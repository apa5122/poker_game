package poker;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.HashMap;

import poker.Message.Action;


public class Sequencer
{
	private HashMap<Long, Message> messages = new HashMap<Long, Message>();
	private MulticastSocket socket;
	
	public Sequencer(MulticastSocket socket)
	{
		this.socket = socket;
	}
	
	public void send(Action action, String text)
	{
		Message message = new Message(nextSequence(), action, text);
		sendMessage(message);
		messages.put(message.sequence, message);
	}
	
	public void resend(long sequence)
	{
		sendMessage(messages.get(sequence));
	}
	
	public void sendMessage(Message message)
	{
		try
		{
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	        ObjectOutputStream out = new ObjectOutputStream(buffer);

	        out.writeObject(message);
	        out.flush();

	        DatagramPacket packet = new DatagramPacket(buffer.toByteArray(),
	        		buffer.toByteArray().length, InetAddress.getByName(Config.multicastHost),
	        		Config.multicastPort);
	        socket.send(packet);
		}
		catch (IOException e)
		{
			// Do something
		}
	}
	
	public long nextSequence()
	{
		return messages.size();
	}
}