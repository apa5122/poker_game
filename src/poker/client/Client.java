package poker.client;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import poker.Config;
import poker.Message;
import poker.State;
import poker.Message.Action;
import poker.cards.Hand;
import poker.server.Server;

public class Client implements Runnable
{
	private Server server;
	private MulticastSocket socket;
	private State state = State.WAITING;
	private long expectedSequence;
	private Message message;
	private int pid;
	private Hand hand;
	
	public Client(int rmiPort) throws IOException, NotBoundException
	{
		// Join multicast group
		socket = new MulticastSocket(Config.multicastPort);
		socket.joinGroup(InetAddress.getByName(Config.multicastHost));
		
		// RMI connection
		server = (Server)Naming.lookup("//" + Config.rmiHost + ":" +
				rmiPort + "/" + Config.serviceName);
	}
	
	public void connect(String name) throws RemoteException
	{
		// Returns a unique player identifier that's used when
		// communicating with the server
		pid = server.connect(name);
		
		if (pid < 0)
			System.out.println("The game has already started");
		else
		{
			// The message announcing us joining the game has already
			// been sent. Thus the expected sequence is that of the
			// last message and not the next message.
			expectedSequence = server.nextSequence() - 1;
			run();
		}
	}
	
	public void run()
	{
		boolean running = true;
		
		while (running)
		{	
			try
			{
				// Deals with the last received message if it has the
				// expected sequence
				if (message != null && message.sequence == expectedSequence)
				{
					System.out.println(message);
					expectedSequence++;
					handleMessage(message);
				}
				// If there are any missing messages, then request that the
				// oldest is resent
				if (server.nextSequence() > expectedSequence)
					server.resend(expectedSequence);
				
				message = receiveMessage();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			catch (ClassNotFoundException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	private void handleMessage(Message message) throws RemoteException
	{
		// The game can only change from WAITING to READY after a player
		// has joined
		if (message.action == Action.PLAYER_JOINS)
		{
			if (server.enoughPlayers())
			{
				System.out.println("Press enter to start the game");
				readKeyboard();
				server.ready(pid);
				state = State.READY;
			}
			
			if (state == State.READY)
				System.out.println("Wait for the other players to get ready");
		}
		else if (message.action == Action.GAME_STARTS)
		{
			System.out.println("Press enter to receive cards");
			hand = server.dealHand(pid);
			state = State.DEALT;
			System.out.println("My hand is:\n" + hand);
			System.out.println("Wait for the other players to receive their cards");
		}
		else if (message.action == Action.CARDS_DEALT)
		{
			System.out.println("How many cards would you like to change? (0-5)");
		}
	}
	
	private Message receiveMessage() throws IOException, ClassNotFoundException
	{
		byte[] buffer = new byte[1024];
		DatagramPacket packet = new DatagramPacket(buffer, 1024);

		socket.receive(packet);
		ByteArrayInputStream stream = new ByteArrayInputStream(packet.getData());
		ObjectInputStream in;
		in = new ObjectInputStream(stream);
			
		return (Message)in.readObject();
	}
	
	private String readKeyboard()
	{
		InputStreamReader is = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(is);
		
		try
		{
			return br.readLine();
		}
		catch (IOException e)
		{
			return "";
		}
	}
}