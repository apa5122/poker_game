package poker.cards;

public class Deck
{
	private Card[] cards;
	private int nextDealtCard;
	
	public Deck()
	{	
		cards = new Card[52];
		
		for (int i = 2; i < 15; i++)
		{
			cards[(i - 2) * 4] = new Card(i, "clubs");
			cards[(i - 2) * 4 + 1] = new Card(i, "diamonds");
			cards[(i - 2) * 4 + 2] = new Card(i, "hearts");
			cards[(i - 2) * 4 + 3] = new Card(i, "spades");
		}
		
		shuffle();
	}
	
	public Card[] getCards()
	{
		return cards;
	}
	
	public Hand dealHand()
	{
		if (isOutOfCards())
		{
			shuffle();
		}
		
		Card[] hand = new Card[5];
		
		for (int i = 0; i < 5; i++)
		{
			hand[i] = cards[nextDealtCard];
			nextDealtCard++;
		}
		
		return new Hand(hand);
	}
	
	private void shuffle()
	{
		Card tempCard;
		int[] test = new int[52];
		
		nextDealtCard = 0;
		
		for (int i = 0; i < 52; i++)
		{
			int randpos = (int) (Math.random() * (52 - i) + i);
			test[i] = randpos;
			tempCard = cards[i];
			cards[i] = cards[randpos];
			cards[randpos] = tempCard;
		}
	}
	
	private boolean isOutOfCards()
	{
		if (nextDealtCard > 47)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}