package poker.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

import poker.Config;
import poker.Player;
import poker.Sequencer;
import poker.State;
import poker.Message.Action;
import poker.cards.Deck;
import poker.cards.Hand;


@SuppressWarnings("serial")
public class ServerImpl extends UnicastRemoteObject implements Server
{
	private HashMap<Integer, Player> players = new HashMap<Integer, Player>();
	private State state = State.WAITING;
	private Sequencer sequencer;
	private Deck deck;
	
	public ServerImpl(int rmiPort) throws IOException
	{		
		// Setup multicast
		MulticastSocket socket = new MulticastSocket(Config.multicastPort);
		socket.joinGroup(InetAddress.getByName(Config.multicastHost));
		socket.setTimeToLive(1);
		
		// Setup RMI
		LocateRegistry.createRegistry(rmiPort);
		Naming.rebind("//" + Config.rmiHost + ":" + rmiPort + "/" + Config.serviceName, this);
		
		sequencer = new Sequencer(socket);
		deck = new Deck();
	}
	
	public int connect(String name)
	{
		int numberOfPlayers = players.size();
		
		if (numberOfPlayers < 100 && state == State.WAITING)
		{
			players.put(numberOfPlayers, new Player(name));
			sequencer.send(Action.PLAYER_JOINS,
					name + " has joined the game");
			
			return numberOfPlayers;
		}
		else
			return -1;
	}
	
	public void resend(long sequence)
	{
		sequencer.resend(sequence);
	}
	
	public long nextSequence()
	{
		return sequencer.nextSequence();
	}
	
	public boolean enoughPlayers()
	{
		return players.size() >= 2;
	}
	
	public void ready(int pid)
	{
		Player player = players.get(pid);
		player.state = Player.State.READY;
		
		if (isReady())
		{
			state = State.READY;
			sequencer.send(Action.GAME_STARTS, "The game has started");
		}
	}
	
	public Hand dealHand(int pid)
	{
		Player player = players.get(pid);
		player.state = Player.State.PLAYING;
		
		if (isPlaying())
		{
			state = State.DEALT;
			sequencer.send(Action.CARDS_DEALT, "");
		}
		
		return deck.dealHand();
	}
	
	private boolean isReady()
	{
		for (Player player : players.values())
			if (player.state == Player.State.WAITING)
				return false;
		
		return true;
	}
	
	private boolean isPlaying()
	{
		for (Player player : players.values())
			if (player.state != Player.State.PLAYING)
				return false;
		
		return true;
	}
}