package poker;

public class Player
{
	public enum State { WAITING, READY, PLAYING }
	private String name;
	public State state = State.WAITING;
	
	public Player(String name)
	{
		this.name = name;
	}
	
	public String toString()
	{
		return name;
	}
}