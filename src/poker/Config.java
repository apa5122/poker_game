package poker;

public class Config
{
	public static final int multicastPort = 5555;
	public static final String multicastHost = "239.192.0.0";
	
	public static final String serviceName = "PokerServer";
	public static final String rmiHost = "localhost";
}