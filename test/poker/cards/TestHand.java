package poker.cards;

import static org.junit.Assert.*;

import org.junit.Test;


public class TestHand {	
	@Test
	public void testCompareTo()
	{
		// Ace high straight flush
		Hand hand1 = new Hand(new Card[]{new Card(14, "spades"), new Card(10, "spades"),
					 new Card(11, "spades"), new Card(13, "spades"), new Card(12, "spades")});
		// Six high straight flush
		Hand hand2 = new Hand(new Card[]{new Card(6, "spades"), new Card(2, "spades"),
					 new Card(3, "spades"), new Card(4, "spades"), new Card(5, "spades")});
		// Ace high flush
		Hand hand3 = new Hand(new Card[]{new Card(14, "spades"), new Card(2, "spades"),
					 new Card(3, "spades"), new Card(4, "spades"), new Card(6, "spades")});
		// Another ace high flush
		Hand hand4 = new Hand(new Card[]{new Card(14, "hearts"), new Card(2, "hearts"),
					 new Card(3, "hearts"), new Card(4, "hearts"), new Card(13, "hearts")});
		// Four of a kind
		Hand hand5 = new Hand(new Card[]{new Card(14, "spades"), new Card(14, "hearts"),
					 new Card(14, "clubs"), new Card(14, "diamonds"), new Card(6, "spades")});
		
		assertEquals(1, hand1.compareTo(hand2));
		assertEquals(1, hand2.compareTo(hand3));
		assertEquals(1, hand1.compareTo(hand5));
		assertEquals(3, hand3.compareTo(hand4));
		assertEquals(2, hand3.compareTo(hand5));
	}
}