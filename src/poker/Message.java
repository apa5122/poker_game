package poker;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Message implements Serializable
{
	public enum Action { PLAYER_JOINS, GAME_STARTS, CARDS_DEALT, PLAYER_BETS, GAME_ENDS }
	public long sequence;
	public Action action;
	public String text;
	
	public Message(long sequence, Action action, String text)
	{
		this.sequence = sequence;
		this.action = action;
		this.text = text;
	}

	public String toString()
	{
		return text;
	}
}